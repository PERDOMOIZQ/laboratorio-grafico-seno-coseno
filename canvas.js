function seno(){
    limpiar();
    var canvas = document.getElementById('canvas');
    var superficie = canvas.getContext('2d');
    superficie.strokeStyle ='black';
    /*.strokeRect hace rectangulos vacios 
    superficie.strokeRect(0,0 ,400, 400);
    superficie.strokeRect(0, 0, 20, 20);   
    */
    //Crea un nuevo camino
    superficie.beginPath();
    //desde moveto hasta lineto  (x,y)
    for (var i=0; i<=400; i=i+20){
        superficie.moveTo(i,0);
        superficie.lineTo(i,400);
      };
      for (var k=0; k<=400; k=k+20){
        superficie.moveTo(0,k);
        superficie.lineTo(400,k);
      };
      superficie.stroke();
    //dibuja la ruta que ha definido con todos esos métodos moveTo () y lineTo
    // se multiplica el 360 que es el periodo por el numero de periodos que quiere
    for (x = 0; x < 360 *2; x += (1 / 1)){
        y = Math.sin(x * 1 * Math.PI / 180) * 100 +200;
        superficie.beginPath();
        // la tercera cambia lo grueso de la linea relacion 1:1
        // la primera cambia la posicion en x
        superficie.arc(x-180, y, 2, 0, 2 * Math.PI, true);
        superficie.fill();
}
}
function coseno(){
    limpiar();
    var canvas = document.getElementById('canvas');
    var superficie = canvas.getContext('2d');
    superficie.strokeStyle ='black';
    /*.strokeRect hace rectangulos vacios 
    superficie.strokeRect(0,0 ,400, 400);
    superficie.strokeRect(0, 0, 20, 20);   
    */
    //Crea un nuevo camino
    superficie.beginPath();
    //desde moveto hasta lineto  (x,y)
    for (var i=0; i<=400; i=i+20){
        superficie.moveTo(i,0);
        superficie.lineTo(i,400);
      };
      for (var k=0; k<=400; k=k+20){
        superficie.moveTo(0,k);
        superficie.lineTo(400,k);
      };
      superficie.stroke();
    //dibuja la ruta que ha definido con todos esos métodos moveTo () y lineTo
    // se multiplica el 360 que es el periodo por el numero de periodos que quiere
    
    for (x = 0; x < 360*2 ; x += (1 )){
        y = Math.cos(x * 1* Math.PI / 180) * 100 +200;
        superficie.beginPath();
        // la tercera cambia lo grueso de la linea relacion 1:1
        superficie.arc(x-180, y, 2, 0, 2 * Math.PI, true);
        superficie.fill();
    }
}

function limpiar() {
    canvas.width = canvas.width;
}